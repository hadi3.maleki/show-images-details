const text = document.querySelector(".detail");
const showAlt = document.querySelectorAll(".showAlt");

showAlt.forEach(item => {
    item.addEventListener("mouseover", () => {
        text.innerText = item.alt;
    }
    )
});

showAlt.forEach(item => {
    item.addEventListener("mouseout", () => {
        text.innerText = "Hover over a sunsign image to display details."
    }
    )
});